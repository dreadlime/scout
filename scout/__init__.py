import psycopg2
from flask import Flask


def create_app():
    # connect to postgresql database
    db = psycopg2.connect(dbname='scout', user='scout',
                          password='scout')
    assert db.status == 1

    app = Flask(__name__)

    @app.route("/")
    def ready():
        return "Teleport succesful."

    return app
