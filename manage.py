from flask_script import Manager, Server
from scout import create_app

manager = Manager(create_app())
manager.add_command('runlocal', Server())

if __name__ == '__main__':
    manager.run()
