Scout
=====

Python Flask application used as a 'hello world' for new server 
testing and PoC deployment

Database
--------
Scout expects a connection to a local postgresql database:
.. code-block:: json
    {
      "dbname": "scout",
      "user": "scout",
      "password": "scout"
    }
